# Creep Front Earthquake Triggering

Simulations in the paper "Creep fronts and complexity in laboratory earthquake sequences illuminate delayed earthquake triggering". 

# Getting Started
## Obtaining uguca

These simulations were implemented and executed on `uguca v0.9`. 

```
git clone git@gitlab.com:uguca/uguca.git -b v0.9
```

Please visit <https://uguca.gitlab.io/uguca/> for more details. 

## Clone this repository into `simulations`

```
cd uguca
mkdir simulations
cd simulations
git clone git@gitlab.com:uguca/projects/creep_front_earthquake_triggering.git
```

## Add this simulation into `CMakeLists.txt`
```
echo "add_subdirectory(creep_front_earthquake_triggering)" >> CMakeLists.txt
```

## Compile using `cmake`
```
cd ..
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DUCA_SIMULATIONS=ON  ..
cmake --build .
```

## Run simulation
First, you will need to generate kernels for nu = 0.35. 
```
cd ../kernels
./laplace_inverse.py 0.35
```
Second, go to where the executables are. 
```
cd ../build/simulations/creep_front_earthquake_triggering
```
Create the folder for simulation output.
```
mkdir output
```
Execute the standard simulation with the example input file ``slowfront7_720.in``.
```
./slowfront7 slowfront7_720.in
```

## Simulations
Input files of relevant simulations are included in this repository (`slowfront7_***.in`), and their input and extracted output parameters are tabulated in `simulations.xlsx`. Figure 3g in the manuscript can be reproduced using the data in `simulations.xlsx`. 

|    Job Name    | $`x_\mathrm{LP}`$ (mm) | $`\tau_0`$ (MPa) |
|:--------------:|:-------------------:|:----------------:|
| slowfront7_683 |	 5                 | 6.25             |
| slowfront7_688 |	 5                 | 6.27             |
| slowfront7_693 |	 5                 | 6.29             |
| slowfront7_698 |	 5                 | 6.31             |
| slowfront7_703 |	 5                 | 6.33             |
| slowfront7_708 |	 5                 | 6.35             |
| slowfront7_713 |	 5                 | 6.37             |
| slowfront7_718 |	 5                 | 6.39             |
| slowfront7_723 |	 5                 | 6.41             |
| slowfront7_728 |	 5                 | 6.43             |
| slowfront7_733 |	 5                 | 6.45             |
| slowfront7_738 |	 5                 | 6.47             |
| slowfront7_684 |	10                 | 6.25             |
| slowfront7_689 |	10                 | 6.27             |
| slowfront7_694 |	10                 | 6.29             |
| slowfront7_699 |	10                 | 6.31             |
| slowfront7_704 |	10                 | 6.33             |
| slowfront7_709 |	10                 | 6.35             |
| slowfront7_714 |	10                 | 6.37             |
| slowfront7_719 |	10                 | 6.39             |
| slowfront7_724 |	10                 | 6.41             |
| slowfront7_729 |	10                 | 6.43             |
| slowfront7_734 |	10                 | 6.45             |
| slowfront7_739 |	10                 | 6.47             |
| slowfront7_685 |	15                 | 6.25             |
| slowfront7_690 |	15                 | 6.27             |
| slowfront7_695 |	15                 | 6.29             |
| slowfront7_700 |	15                 | 6.31             |
| slowfront7_705 |	15                 | 6.33             |
| slowfront7_710 |	15                 | 6.35             |
| slowfront7_715 |	15                 | 6.37             |
| slowfront7_720 |	15                 | 6.39             |
| slowfront7_725 |	15                 | 6.41             |
| slowfront7_730 |	15                 | 6.43             |
| slowfront7_735 |	15                 | 6.45             |
| slowfront7_740 |	15                 | 6.47             |
| slowfront7_686 |	20                 | 6.25             |
| slowfront7_691 |	20                 | 6.27             |
| slowfront7_696 |	20                 | 6.29             |
| slowfront7_701 |	20                 | 6.31             |
| slowfront7_706 |	20                 | 6.33             |
| slowfront7_711 |	20                 | 6.35             |
| slowfront7_716 |	20                 | 6.37             |
| slowfront7_721 |	20                 | 6.39             |
| slowfront7_726 |	20                 | 6.41             |
| slowfront7_731 |	20                 | 6.43             |
| slowfront7_736 |	20                 | 6.45             |
| slowfront7_741 |	20                 | 6.47             |
| slowfront7_687 |	25                 | 6.25             |
| slowfront7_692 |	25                 | 6.27             |
| slowfront7_697 |	25                 | 6.29             |
| slowfront7_702 |	25                 | 6.31             |
| slowfront7_707 |	25                 | 6.33             |
| slowfront7_712 |	25                 | 6.35             |
| slowfront7_717 |	25                 | 6.37             |
| slowfront7_722 |	25                 | 6.39             |
| slowfront7_727 |	25                 | 6.41             |
| slowfront7_732 |	25                 | 6.43             |
| slowfront7_737 |	25                 | 6.45             |
| slowfront7_742 |	25                 | 6.47             |

