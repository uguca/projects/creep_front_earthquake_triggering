/**
 * @file slowfront7.cc
 *
 * @author Chun-Yu Ke <ck659@cornell.edu>
 *
 * @date creation: Sun May 20 2018
 * @date last modification: Mon Sep 05 2022
 *
 * @brief
 *
 * @section LICENSE
 *
 * MIT License
 * Copyright (c) 2018 Chun-Yu Ke
 *
 */

#include <iostream>
#include <fstream>
#include <math.h>
#include <unistd.h>

#include <sys/time.h>

#include "static_communicator_mpi.hh"

#include "unimat_shear_interface.hh"
#include "material.hh"
#include "precomputed_kernel.hh"
#include "rate_and_state_law.hh"

#include "uca_parameter_reader.hh"

using namespace uguca;

std::vector<std::string> &split(const std::string &s,
				char delim,
				std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

int main(int argc, char* argv[]) {
  int world_rank = StaticCommunicatorMPI::getInstance()->whoAmI();
  
  // ---------------------
  // input file
  // ---------------------

  std::string fname;
  if(argc < 2) {
    if (world_rank == 0) {
      std::cerr << "Not enough arguments:" 
      << " ./slowfront <input_file>" << std::endl;
    }
    return 0;
  }
  else {
    fname = argv[1];
  }
  if (world_rank == 0)
    std::cout << "simulation_code = uguca" << std::endl;

  // ---------------------------------------------------------------------------

  // Read input data file
  ParameterReader data;
  data.readInputFile(fname);

  std::string bname = data.get<std::string>("simulation_name");
  std::string dump_folder = data.get<std::string>("dump_folder");
  if (world_rank == 0)
    std::cout << "output_folder = " << dump_folder << std::endl;
  
  // copy input file
  if (world_rank==0) {
    std::ifstream src_ifile(fname, std::ios::binary);
    std::ofstream dst_ifile(dump_folder + bname + ".in", std::ios::binary);
    dst_ifile << src_ifile.rdbuf();
    src_ifile.close();
    dst_ifile.close();
  }

  // ---------------------------------------------------------------------------
  // parameters
  double length_fault = data.get<double>("length_fault"); // 0.720;
  double domain_factor = data.get<double>("domain_factor"); // 4.0;
  unsigned nb_nodes_x = data.get<unsigned>("nb_nodes"); //  360;
  double duration = data.get<double>("duration"); // 0.25;
  double dump_int = data.get<double>("dump_int"); // duration / 1000;
  double time_step_factor = data.get<double>("time_step_factor"); //  0.35;
  unsigned n_pc = data.get<unsigned>("n_pc"); // 1;

  // material
  double E = data.get<double>("E"); // 3000000000.0;
  double nu = data.get<double>("nu"); // 0.35;
  double rho = data.get<double>("rho"); // 1200.0;

  // rate and state
  double a_default = data.get<double>("a_default"); // 0.010; //0.008
  double b_default = data.get<double>("b_default"); // 0.011; //0.012
  double delta_a_0 = data.get<double>("delta_a_0"); // 0.020; //0.008
  double D_c = data.get<double>("D_c"); // 2e-6;
  double delta_dot_0 = data.get<double>("delta_dot_0"); // 1.0e-6;
  double f_0 = data.get<double>("f_0"); // 0.6;
  unsigned rsf_law_id = data.get<unsigned>("rsf_law_id"); // 2;

  // initial conditions
  double normal_load = data.get<double>("normal_load");
  double tau_ext = data.get<double>("tau_ext");
  double delta_dot_init = data.get<double>("delta_dot_init");
  double theta_init = data.get<double>("theta_init");

  // nucleation
  double tau_0 = data.get<double>("tau_0");
  double tau_1 = data.get<double>("tau_1");
  double tau_2 = data.get<double>("tau_2");
  double R1 = data.get<double>("R1"); // 0.34;
  double R2 = data.get<double>("R2"); // 0.34;
  double T = data.get<double>("T"); // 1e-3;  // 1.0;
  double loading_rate = data.get<double>("loading_rate"); // 10.0e6;

  // ---------------------------------------------------------------------------
  // create mesh
  
  double length_x = domain_factor * length_fault;
  Mesh mesh(length_x, nb_nodes_x);
  const std::vector<NodalField*> coords = mesh.getCoords();

  // ---------------------------------------------------------------------------
  // constitutive interface law
  RateAndStateLaw::EvolutionLaw rsf_law = rsf_law_id == 1 ? 
    RateAndStateLaw::EvolutionLaw::AgingLaw : RateAndStateLaw::EvolutionLaw::SlipLaw;
  RateAndStateLaw law(mesh, a_default, b_default, D_c, delta_dot_0,
                      f_0, theta_init, std::abs(normal_load), rsf_law);
  NodalField* theta = law.getTheta();
  NodalField* a = law.getA();
  NodalField* b = law.getB();

  Material mat = Material(E, nu, rho);
  mat.readPrecomputedKernels();
 

  // ---------------------------------------------------------------------------
  // weak interface

  UnimatShearInterface interface(mesh, mat, law);

  // ---------------------------------------------------------------------------
  // initial conditions

  // init external load
  NodalField* ext_shear = interface.getShearLoad();
  NodalField* ext_normal = interface.getNormalLoad();
  NodalField* ext_normal_rsf = law.getSigma();
  ext_shear->setAllValuesTo(tau_ext);
  ext_normal->setAllValuesTo(normal_load);
  ext_normal_rsf->setAllValuesTo(normal_load);

  for (int i = 0; i < mesh.getNbNodes(); ++i) {
    double x = (*coords[0])(i) - length_x / 2;
    (*ext_normal)(i) = normal_load;
    (*ext_normal_rsf)(i) = std::abs((*ext_normal)(i));

    if (x >= -length_fault / 2 - R1 && x <= -length_fault / 2 + R1) {
      (*ext_shear)(i) = tau_1;
    } else if (x > -length_fault / 2 + R1 && x < length_fault / 2 - R2) {
      (*ext_shear)(i) = tau_0;
    } else if (x >= length_fault / 2 - R2 && x <= length_fault / 2 + R2) {
      (*ext_shear)(i) = tau_2;
    } else {
      (*ext_shear)(i) = tau_ext;
    }
  }

  // init velocity
  HalfSpace& top = interface.getTop();
  NodalField* velo0_top = top.getVelo(0);

  for (int i = 0; i < mesh.getNbNodes(); ++i) {
    (*velo0_top)(i) = delta_dot_init / 2;
  }

  // init theta
  for (int i = 0; i < mesh.getNbNodes(); ++i) {
    (*theta)(i) =
        D_c / delta_dot_0 *
        std::exp(
            ((*a)(i)*std::log(2 * std::sinh(
              (*ext_shear)(i) / (*a)(i) / std::abs((*ext_normal)(i))
            )) - f_0 - (*a)(i)*std::log(delta_dot_init / delta_dot_0)
          ) / (*b)(i));
  }

  // time step
  double time_step = time_step_factor * interface.getStableTimeStep();
  interface.setTimeStep(time_step);
  unsigned nb_time_steps = std::ceil(duration / time_step);

  // init interface
  interface.initPredictorCorrector(n_pc);
  law.init();
  interface.init(true);
  

  // ---------------------------------------------------------------------------
  // dumping
  std::string dumper_bname = bname;
  std::string bname_sep = "-";
  std::string dumper_group_interface = "interface";
  if (world_rank == 0) {
    std::cout << "dumper_bname = " << dumper_bname << std::endl;
    std::cout << "bname_sep = " << bname_sep << std::endl;
    std::cout << "dumper_group = " << dumper_group_interface << std::endl;
    std::cout << "dump int = " << dump_int << std::endl;
  }
  interface.initDump(dumper_bname + bname_sep + dumper_group_interface, dump_folder, Dumper::Format::Binary);

  std::string dump_fields = data.get<std::string>("dump_fields");
  std::vector<std::string> dump_fields_sep = split(dump_fields,',');
  for (std::vector<std::string>::iterator it = dump_fields_sep.begin();
       it != dump_fields_sep.end(); ++it) {
    interface.registerDumpField(*it);
  }
  interface.dump(0,0);

  unsigned s_dump = dump_int / time_step + 1;

  if (world_rank == 0) 
    std::cout << "simulation start..." << std::endl;
  
  // time stepping
  for (unsigned s = 1; s <= nb_time_steps; ++s) {
    if (world_rank == 0) {
      std::cout << "s=" << s << "/" << nb_time_steps << "\r";
      std::cout.flush();
    }
    // nucleation
    double t = time_step * s;

    // ext_shear->setAllValuesTo(shear_load + loading_rate * t);
    for (int i = 0; i < mesh.getNbNodes(); ++i) {
      double x = (*coords[0])(i) - length_x / 2;
      if (x >= -length_fault / 2 - R1 && x <= -length_fault / 2 + R1) {
        (*ext_shear)(i) = tau_1 + loading_rate * t;
      } else if (x > -length_fault / 2 + R1 && x < length_fault / 2 - R2) {
        (*ext_shear)(i) = tau_0 + loading_rate * t;
      } else if (x >= length_fault / 2 - R2 && x <= length_fault / 2 + R2) {
        (*ext_shear)(i) = tau_2 + loading_rate * t;
      } else {
        (*ext_shear)(i) = tau_ext + loading_rate * t;
      }
    }

    // time integration
    interface.advanceTimeStep();

    // dump
    if (world_rank == 0 && s % s_dump == 0) 
      interface.dump(s, s * time_step);
  }

  StaticCommunicatorMPI::getInstance()->finalize();

  if (world_rank == 0)
    std::cout << "weak-interface simulation completed." << std::endl;

  return 0;
}
